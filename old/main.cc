#include <sys/socket.h>
#include <sys/types.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "sock.h"
#include <bits/stdc++.h>
#include <unistd.h>
#include <fcntl.h>
using namespace std;

unsigned int SLEEP_TIME = 50000;
int i = 0;

// this nodes neighbor information
// MAC addr, hop, sequence number
typedef map<string, pair<int, int> > neighbor_map; 
neighbor_map my_nmap;
string my_mac = "FFAAFFAAFFAA";
int my_seq = 0;

void printMac(unsigned char mac[6]) {
   char cc[50*6];
   sprintf(cc, "%02X:%02X:%02X:%02X:%02X:%02X",
	   mac[0], mac[1], mac[2],
	   mac[3], mac[4], mac[5]);
   cout << cc;
}

void receive() {
   cout << "RECEIVER" << (i+1)%10 << endl;
   i++;
   usleep(SLEEP_TIME);
   
   struct Ifconfig ifc;
   init(&ifc);

   printf("This Pi is ");
   /* print the MAC addr */
   int i;
   for (i=0; i<MACADDRLEN-1; i++) {
      printf("%02X:", ifc.mac[i]);
   }
   printf("%02X\n", ifc.mac[MACADDRLEN-1]);

   // use this function to print a mac address given the Ifconfig
   // mac (unsigned char array of size MACADDRLEN) 
   printMac(ifc.mac);
   cout << endl;
   
   /* setting up the socket address structure for the source of the packet */
   struct sockaddr_ll from;
   socklen_t fromlen = sizeof(from);
   
   /* setting up the buffer or receiving */
   char * buf = (char*)malloc(ifc.mtu);

   if (buf == NULL)
      die("Cannot allocate receiving buffer\n");

   fcntl(ifc.sockd, F_SETFL, fcntl(ifc.sockd, F_GETFL) | O_NONBLOCK);
   
   while (1) {
      int recvlen = recvfrom(ifc.sockd, buf, ifc.mtu, 0,
			     (struct sockaddr*) &from, &fromlen);
      
      // nothing received
      if (recvlen < 0) {
	 printf("Cannot receive data: %s\n", strerror(errno));
         return;	 
      } else {

	 string str = buf;
	 istringstream iss(buf);
	 string s, src_mac;
	 getline(iss, src_mac, ',');
	 
	 // if not found, add as 1 hop neighbor, assign my sequence number
	 auto it = my_nmap.find(src_mac);
	 if (it == my_nmap.end()) {
	    cout << "Node: " << my_mac << " discovered new 1 hop neighbor: "
		 << src_mac << ", seq: " << my_seq << endl;
	    my_nmap.insert(make_pair(src_mac, make_pair(1, my_seq)));
	 }

	 // read rest of message and compare with current values in my_nmap
	 while (getline(iss, s, ',')) {
	    string mac = s;
	    getline(iss, s, ',');
	    int seq = stoi(s);
	    // if this mac address is not myself
	    if (mac != my_mac) {	    
	       // if not found, add as 2 hop neighbor, assign received
	       // sequence number.	    
	       auto it = my_nmap.find(mac);
	       if (it == my_nmap.end()) {
		  cout << "Node: " << my_mac << " discovered new 2 hop neighbor: "
		       << mac << ", seq: " << my_seq
		       << ", which goes through 1 hop neighbor: "
		       << src_mac << endl;
		  my_nmap.insert(make_pair(mac, make_pair(2, seq)));
	       }
	    }
	 }
      }
   }

   /* free resources */
   free(buf);
   destroy(&ifc);
}

void sender() {
   cout << "\tSENDER" << endl;
}

int main() {

   time_t prev, next;   
   time(&prev);

   while (1) {

      time(&next);
      
      receive();

      if (next == prev+1) {
	 prev = next;
	 sender();
      }

   }


   return 0;
}
