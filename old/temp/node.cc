#include "WakeupEvent.h"
#include "Simulator.h"
#include "Node.h"
#include "config.h"
#include <string>
#include <iostream>
#include <iomanip>
#include <chrono>
#include <random>

using std::cout;
using std::endl;

Node::Node(Simulator * sim, int id) {
   _simulator = sim;
   _id=id;
}

Node::~Node() {
   _simulator = nullptr;
}


float Node::getCrtTime() {
   if (_simulator != nullptr)
      return _simulator->getCrtTime();
   else {
      cout << "error, crttime returned 0 in Node" << endl;
      return 0;
   }
}


void Node::sleep(boost::coroutines2::coroutine<void>::push_type &contextSwitch,
		 float deltat) {
   // create the WakeupEvent
   if (deltat <= 0)  // in case the deltat is invalid, do nothing
      return;
   if (_simulator == nullptr)  // in case Node is not bound to a simulator, do nothing
      return;

   Event * ev = new WakeupEvent(_id, _simulator, getCrtTime()+deltat);
   _simulator->insertEvent(ev);
   // we now need to sleep so control flow switches to the simulator.
   // When Simulator processes the packet and calls this node again,
   // execution will resume from here.
   contextSwitch();
}

// the CSMA algorithm should go in here
// This is the MAIN function for the code that runs on the nodes of the network
void Node::operator()(boost::coroutines2::coroutine<void>::push_type &contextSwitch) {
   
}













void Node::print(std::string str) {
   cout << "(node:" << _id << ", time:" << std::setprecision(8) << std::fixed << (double)getCrtTime() << ")\t";
   cout << str << endl;
}

void Node::print(std::string str, int i) {
   cout << "(node:" << _id << ", time:" << getCrtTime() << ")\t";
   cout << str << " " << i << endl;   
}

void Node::print(std::string str, double i) {
   cout << "(node:" << _id << ", time:" << std::setprecision(8) << std::fixed << (double)getCrtTime() << ")\t";
   cout << str << " " << i << endl;   
}

double Node::nextTime() {
   // returns time units in seconds
   return (-logf(1.0f - (double)random() / (RAND_MAX + 1.0f)) / FRAME_ARRIVAL_RATE);
}

int Node::backoffValue(int n) {
   unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
   std::minstd_rand0 generator(seed);
   int window = std::pow(2, n);
   return generator() % window;
}
