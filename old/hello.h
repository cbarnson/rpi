#ifndef __HELLO_H
#define __HELLO_H

#include <bitset>

// constants

// emission intervals
const double HELLO_INTERVAL = 2.0;    // seconds
const double REFRESH_INTERVAL = 2.0;  // seconds
const double TC_INTERVAL = 5.0;       // seconds
const double MID_INTERVAL = TC_INTERVAL;
const double HNA_INTERVAL = TC_INTERVAL;

// holding times
const double NEIGHB_HOLD_TIME = 3 * REFRESH_INTERVAL;
const double TOP_HOLD_TIME = 3 * TC_INTERVAL;
const double DUP_HOLD_TIME = 30.0;    // seconds
const double MID_HOLD_TIME = 3 * MID_INTERVAL;
const double HNA_HOLD_TIME = 3 * HNA_INTERVAL;

// for computing validity time "vtime" and "htime" fields
const double SCALING_FACTOR = 0.0625; // seconds


// structures
struct MessageTypes {
   enum type {
      HELLO_MESSAGE = 1,
      TC_MESSAGE = 2,
      MID_MESSAGE = 3,
      HNA_MESSAGE = 4
   };
};

struct LinkTypes {
   enum type {
      UNSPEC_LINK = 0,
      ASYM_LINK = 1,
      SYM_LINK = 2,
      LOST_LINK = 3
   };
};

struct NeighborTypes {
   enum type {
      NOT_NEIGH = 0,
      SYM_NEIGH = 1,
      MPR_NEIGH = 2
   };
};

struct Willingness {
   enum type {
      WILL_NEVER = 0,
      WILL_LOW = 1,
      WILL_DEFAULT = 3,
      WILL_HIGH = 6,
      WILL_ALWAYS = 7
   };
};



// functions
double vtime_to_double(unsigned short t);
unsigned short double_to_vtime(double t);




#endif
