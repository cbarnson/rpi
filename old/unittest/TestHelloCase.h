#ifndef __TESTHELLO_H
#define __TESTHELLO_H

#include <iostream>
#include <string>

#include "cppunit/TestCase.h"
#include "cppunit/TestSuite.h"
#include "cppunit/TestCaller.h"
#include "cppunit/TestRunner.h"
#include "cppunit/Test.h"

class TestHelloCase : public TestCase {

  public:
  TestHelloCase():TestCase() { }
   void testConstructor();
   static Test *suite();

};

#endif
