#include "TestHelloCase.h"

void TestHelloCase::testConstructor() {
   assertEquals(1, 1);
}

Test *TestHelloCase::suite() {
   TestSuite *testSuite = new TestSuite("TestHelloCase");
   // add tests
   testSuite->addTest(new TestCaller(
			 "testConstructor", &TestHelloCase::testConstructor));
   return testSuite;
}
