#include <bits/stdc++.h>
#include "hello.h"
#include "TestHelloCase.h"
#include "cppunit/TestCase.h"
#include "cppunit/TestSuite.h"
#include "cppunit/TestCaller.h"
#include "cppunit/TestRunner.h"
#include "cppunit/Test.h"
using namespace std;

// // emission intervals
// const double HELLO_INTERVAL = 2.0;    // seconds
// const double REFRESH_INTERVAL = 2.0;  // seconds
// const double TC_INTERVAL = 5.0;       // seconds
// const double MID_INTERVAL = TC_INTERVAL;
// const double HNA_INTERVAL = TC_INTERVAL;

// // holding times
// const double NEIGHB_HOLD_TIME = 3 * REFRESH_INTERVAL;
// const double TOP_HOLD_TIME = 3 * TC_INTERVAL;
// const double DUP_HOLD_TIME = 30.0;    // seconds
// const double MID_HOLD_TIME = 3 * MID_INTERVAL;
// const double HNA_HOLD_TIME = 3 * HNA_INTERVAL;

// // for computing validity time "vtime" and "htime" fields
// const double SCALING_FACTOR = 0.0625; // seconds

// double vtime_to_double(unsigned short t) {
//    int b = (int)(bitset<4>(t).to_ulong());
//    int a = (int)(t >> 4);
//    // to be safe, pow unpredictable...
//    int bb = 1;
//    for (int i = 0; i < b; i++)
//       bb *= 2;   
//    return (SCALING_FACTOR * (1.0 + ((double)a / 16)) * (double)bb);
// }

// // given time t, which we want to represent as the vtime mantissa/exponent,
// // compute the 8 bit representation in the form of an unsigned char and return
// // the value
// unsigned short double_to_vtime(double t) {
//    assert(t >= 0);
//    unsigned int b = 0;
//    double tc_ratio = t / SCALING_FACTOR;
//    while (tc_ratio >= pow(2, b)) {
//       b++;
//    }
//    // want largest that satisfies the above, decrement once
//    b--;
//    // compute 16*(T/(C*(2^b))-1) and round up, this is 'a'
//    double temp = ceil(16.0 * (t / (SCALING_FACTOR * ((double)pow(2, b))) - 1.0));
//    unsigned int a = temp;
//    // if 'a' is equal to 16, increment 'b' by one, and set 'a' to 0
//    if (a == 16) {
//       b++;
//       a = 0;
//    }
//    // check
//    assert(a >= 0 && a < 16);
//    assert(b >= 0 && b < 16);

//    unsigned char c = 0;
//    c |= (a << 4);
//    c |= b;
//    return c;
// }

// // ENUMERATIONS
// struct MessageTypes {
//    enum type {
//       HELLO_MESSAGE = 1,
//       TC_MESSAGE = 2,
//       MID_MESSAGE = 3,
//       HNA_MESSAGE = 4
//    };
// };

// struct LinkTypes {
//    enum type {
//       UNSPEC_LINK = 0,
//       ASYM_LINK = 1,
//       SYM_LINK = 2,
//       LOST_LINK = 3
//    };
// };

// struct NeighborTypes {
//    enum type {
//       NOT_NEIGH = 0,
//       SYM_NEIGH = 1,
//       MPR_NEIGH = 2
//    };
// };

// struct Willingness {
//    enum type {
//       WILL_NEVER = 0,
//       WILL_LOW = 1,
//       WILL_DEFAULT = 3,
//       WILL_HIGH = 6,
//       WILL_ALWAYS = 7
//    };
// };

int main(int argc, char* argv[]) {

   if (argc != 2) {
      cout << "usage: tester name-of-class-being-test" << endl;
      exit(1);
   }

   TestRunner runner;
   runner.addTest(argv[1], TestHelloCase::suite());
   runner.run(argc, argv);
   
   // usage of enum
   // MessageTypes::type mt = MessageTypes::HELLO_MESSAGE;
   // if (mt == 1) {
   //    cout << "mt is HELLO_MESSAGE" << endl;
   // }

   // const int NN = 4;
   // double a[NN] = { 2.0, 6.0, 15.0, 30.0 };
   // for (int i = 0; i < NN; i++) {
   //    unsigned short us_vtime = double_to_vtime(a[i]);
   //    cout << "us_vtime: " << bitset<8>(us_vtime) << ", ";
   //    double d_vtime = vtime_to_double(us_vtime);
   //    cout << "d_vtime: " << d_vtime << endl;
   // }

   
   return 0;
}
