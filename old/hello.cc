#include <cmath>
#include <cassert>
#include "hello.h"
using namespace std;

double vtime_to_double(unsigned short t) {
   int b = (int)(bitset<4>(t).to_ulong());
   int a = (int)(t >> 4);
   // to be safe, pow unpredictable...
   int bb = 1;
   for (int i = 0; i < b; i++)
      bb *= 2;   
   return (SCALING_FACTOR * (1.0 + ((double)a / 16)) * (double)bb);
}


// given time t, which we want to represent as the vtime mantissa/exponent,
// compute the 8 bit representation in the form of an unsigned char and return
// the value
unsigned short double_to_vtime(double t) {
   assert(t >= 0);
   unsigned int b = 0;
   double tc_ratio = t / SCALING_FACTOR;
   while (tc_ratio >= pow(2, b)) {
      b++;
   }
   // want largest that satisfies the above, decrement once
   b--;
   // compute 16*(T/(C*(2^b))-1) and round up, this is 'a'
   double temp = ceil(16.0 * (t / (SCALING_FACTOR * ((double)pow(2, b))) - 1.0));
   unsigned int a = temp;
   // if 'a' is equal to 16, increment 'b' by one, and set 'a' to 0
   if (a == 16) {
      b++;
      a = 0;
   }
   // check
   assert(a >= 0 && a < 16);
   assert(b >= 0 && b < 16);

   unsigned char c = 0;
   c |= (a << 4);
   c |= b;
   return c;
}
