#ifndef _TYPES_H
#define _TYPES_H

// structures
struct MessageTypes {
   enum type {
      HELLO_MESSAGE = 1,
      TC_MESSAGE = 2,
      MID_MESSAGE = 3,
      HNA_MESSAGE = 4
   };
};

struct LinkTypes {
   enum type {
      UNSPEC_LINK = 0,
      ASYM_LINK = 1,
      SYM_LINK = 2,
      LOST_LINK = 3
   };
};

struct NeighborTypes {
   enum type {
      NOT_NEIGH = 0,
      SYM_NEIGH = 1,
      MPR_NEIGH = 2
   };
};



struct Willingness {
   enum type {
      WILL_NEVER = 0,
      WILL_LOW = 1,
      WILL_DEFAULT = 3,
      WILL_HIGH = 6,
      WILL_ALWAYS = 7
   };
};


#endif
