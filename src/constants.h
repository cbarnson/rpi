#ifndef _CONSTANTS_H
#define _CONSTANTS_H

// constants

// emission intervals
const double HELLO_INTERVAL = 2.0;    // seconds
const double REFRESH_INTERVAL = 2.0;  // seconds
const double TC_INTERVAL = 5.0;       // seconds
const double MID_INTERVAL = TC_INTERVAL;
const double HNA_INTERVAL = TC_INTERVAL;

// holding times
const double NEIGHB_HOLD_TIME = 3 * REFRESH_INTERVAL;
const double TOP_HOLD_TIME = 3 * TC_INTERVAL;
const double DUP_HOLD_TIME = 30.0;    // seconds
const double MID_HOLD_TIME = 3 * MID_INTERVAL;
const double HNA_HOLD_TIME = 3 * HNA_INTERVAL;

// for computing validity time "vtime" and "htime" fields
const double SCALING_FACTOR = 0.0625; // seconds


#endif
