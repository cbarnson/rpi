#ifndef _SHARED_H
#define _SHARED_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <ctype.h>

#define SIZE(a) (sizeof(a)/sizeof(*a))
#define LEN(a) (sizeof(a)/sizeof(*a) - 1)
#define UNUSED(a) (void)(a)
#define ZEROM(a) memset(&(a), 0, sizeof(a))

#define LINE fprintf(stderr, "%s: %d %s\n", __FILE__, __LINE__, __func__); fflush(stderr)
#define MACROSTR_IMPL(a) #a
#define MACROSTR(a) MACROSTR_IMPL(a)

#define CHK(a) do { \
	if((a) == -1) { \
		fprintf(stderr, \
			__FILE__ ":" MACROSTR(__LINE__) "   " #a "   failed: %s\n" \
		, strerror(errno)); \
		exit(1); \
	} \
} while(0);
#define CHK0(a) do { \
	if((a) == 0) { \
		fprintf(stderr, \
			__FILE__ ":" MACROSTR(__LINE__) "   " #a "   failed: %s\n" \
		, strerror(errno)); \
		exit(1); \
	} \
} while(0);
#define CHKN0(a) do { \
	if((a) != 0) { \
		fprintf(stderr, \
			__FILE__ ":" MACROSTR(__LINE__) "   " #a "   failed: %s\n" \
		, strerror(errno)); \
		exit(1); \
	} \
} while(0);

   
enum {
   HELLO_MESSAGE = 1,
   TC_MESSAGE = 2,
   MID_MESSAGE = 3,
   HNA_MESSAGE = 4
};

enum {
   UNSPEC_LINK = 0,
   ASYM_LINK = 1,
   SYM_LINK = 2,
   LOST_LINK = 3
};
   
enum {
   NOT_NEIGH = 0,
   SYM_NEIGH = 1,
   MPR_NEIGH = 2
};


enum {
   WILL_NEVER = 0,
   WILL_LOW = 1,
   WILL_DEFAULT = 3,
   WILL_HIGH = 6,
   WILL_ALWAYS = 7
};


#endif
