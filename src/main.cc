// Cody Barnson
// main() for raspberry pi

// #include <bits/stdc++.h>
#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <wchar.h>
#include <stdio.h>
// #include <string.h>
#include <assert.h>
#include <signal.h>
#include <time.h>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>

// c++
#include <iostream>
#include <string>
#include <iterator>
#include <list>

#include "vtime.h"
#include "shared.h"
//#include "types.h"

using namespace std;

// local information database

// info used when declaring the neighbor interfaces in the HELLO messages
struct link_tuple {
  uint32_t L_local_iface_addr; // interface address of local node (one endpoint of the link)
  uint32_t L_neighbor_iface_addr; // interface address neighbor node (other endpoint of link)
  uint32_t L_SYM_time; // time until which the link is considered symmetric
                       // used to decide the link type declared for the neighbor interface
                       // if not expired, the link MUST be declared symmetric
                       // if expired, MUST be declared asymmetric
  uint32_t L_ASYM_time; // time until which the neighbor interface is considered heard
  uint32_t L_time; // time this record expires and must be removed

  // when l_sym_time and l_asym_time are expired, the link is considered lost
};
list<link_tuple> link_set;

// info describing neighbors
struct neighbor_tuple {
  uint32_t N_neighbor_main_addr; // main address of a neighbor
  uint32_t N_status; // specifies if NOT_SYM or SYM
  uint8_t N_willingness; // willingness to carry traffic on behalf of other nodes
};
list<neighbor_tuple> neighbor_set;

// info describing symmetric links between its neighbors and the symmetric 2-hop neighborhood
struct two_hop_tuple {
  uint32_t N_neighbor_main_addr; // main address of the neighbor
  uint32_t N_2hop_addr; // main address of a 2-hop neighbor with symmetric link to N_neighbor_main_addr
  uint32_t N_time; // time at which the tuple expires and MUST be removed
};
list<two_hop_tuple> two_hop_neighbor_set;

//====================
// not required for now
//====================
struct mpr_selector_tuple {
  uint32_t MS_main_addr; // main address of a node which has selected this node as mpr
  uint32_t MS_time; // time at which the tuple expires and MUST be removed
};
list<mpr_selector_tuple> mpr_selector_set;

//====================
// not required for now
//====================
struct topology_tuple {
  uint32_t T_dest_addr; // main address of a node reachable in one hop from the node with main addr T_last_addr
  uint32_t T_last_addr; // is mpr of T_dest_addr
  uint32_t T_seq; 
  uint32_t T_time; // time this tuple expires and MUST be removed
};
list<topology_tuple> topology_set;


// struct link_neighbor {
//    uint8_t link_code;
//    uint8_t reserved;
//    uint16_t link_msg_size;
//    // list of neighbor interface addresses
//    uint32_t *neighbor_addr;
// };

// struct hello_msg {
//    uint16_t reserved; // 0's
//    uint8_t htime;
//    uint8_t willingness; // WILL_DEFAULT
//    // each 'array' entry has different link code
//    link_neighbor *neighbor_list;
// };

struct hello_msg {
  
};

struct hello_msg_header {
  uint16_t reserved;
  uint8_t htime;
  uint8_t willingness;
  hello_msg *hello_list;
};

struct olsr_msg {
   uint8_t msg_type;
   uint8_t vtime;
   uint16_t msg_size;
   uint32_t originator;
   uint8_t ttl;
   uint8_t hop_count;
   uint16_t msg_seq_num;
   // only considering hello messages,
   // so msg_type will equal HELLO_MESSAGE
  hello_msg_header message;
   // hello_msg message;   
};

struct olsr_msg_header {
   uint16_t packet_length;
   uint16_t packet_seq_num;
   // packet contents
   olsr_msg *message;
};



// log file
FILE* flog = NULL;


struct duplicate_tuple {
   uint32_t d_addr;
   uint16_t d_seq_num;
   bool d_retransmitted;
   list<uint32_t> d_iface_list;
   uint32_t d_time;

   duplicate_tuple(uint32_t a, uint16_t s, bool r, uint32_t t) :
      d_addr(a), d_seq_num(s), d_retransmitted(r), d_time(t) { }
};

struct duplicate_set {
   list<duplicate_tuple> d_list;
};

duplicate_set my_duplicate_set;
uint32_t my_main_addr;

// does addr and seq number exist in my_duplicate_set
bool exists(int32_t addr, uint16_t seq_num) {
   list<duplicate_tuple> *d_list = &my_duplicate_set.d_list;
   for (auto it = d_list->begin(); it != d_list->end(); ++it) {
      if (it->d_addr == addr && it->d_seq_num == seq_num)
	 return true;	 
   }
   return false;
}

void parse_packet(olsr_msg_header omh) {
   int plen = omh.packet_length;
   int pseq = omh.packet_seq_num;
   if (plen < 16) return;
   
   olsr_msg *om;
   int i = 0;

   while (om = (omh.message + i++)) {
      // if this message addr and sequence number is in my duplicate set,
      // don't process
      if (om->msg_type != HELLO_MESSAGE ||
	  om->ttl < 0 || // because all messages sent will have hop count of 1
	  om->originator == my_main_addr ||
	  exists(om->originator, om->msg_seq_num)) {
	 continue;
      }

      
      
   }
   
}

int main() {


   // open log file
   flog = fopen("rpi.log", "w");
   assert(flog);

   // write something to log
   fprintf(flog, "testing write to log %i\n", 1);

   duplicate_tuple dt(1000, 1, false, 5);
   my_duplicate_set.d_list.push_front(dt);

   if (exists(100, 1)) {
      cout << "exists" << endl;
   } else {
      cout << "not exists" << endl;
   }
   
   // close log file
   CHKN0(fclose(flog));
   printf("\nexited successfully.\n");

   // const int NN = 4;
   // double a[NN] = { 2.0, 6.0, 15.0, 30.0 };
   // for (int i = 0; i < NN; i++) {
   //    unsigned short us_vtime = double_to_vtime(a[i]);
   //    cout << "us_vtime: " << bitset<8>(us_vtime) << ", ";
   //    double d_vtime = vtime_to_double(us_vtime);
   //    cout << "d_vtime: " << d_vtime << endl;
   // }

   return 0;
}





