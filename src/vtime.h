#ifndef _VTIME_H
#define _VTIME_H

#include <stdint.h>

// functions
double vtime_to_double(uint8_t t);
uint8_t double_to_vtime(double t);

#endif 
