/* First we set up a typical server-side socket by using the socket(2) and listen(2) system calls. Then we pass this socket along with our "action" function to the olsrd socket parser. */


void
plugin_ipc_init()
{
  struct sockaddr_in sin;

  /* Init ipc socket */
  if ((ipc_socket = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
    {
      perror("IPC socket");
    }
  else
    {
      /* Bind the socket */
      
      /* complete the socket structure */
      memset(&sin, 0, sizeof(sin));
      sin.sin_family = AF_INET;
      sin.sin_addr.s_addr = INADDR_ANY;
      sin.sin_port = htons(9999);
      
      /* bind the socket to the port number */
      if (bind(ipc_socket, (struct sockaddr *) &sin, sizeof(sin)) == -1) 
      {
        perror("IPC bind");
	return;
      }
      
      /* show that we are willing to listen */
      if (listen(ipc_socket, 1) == -1) 
      {
        perror("IPC listen");
	return;
      }


      /* Register with olsrd */
      add_olsr_socket(ipc_socket, &ipc_action);

    }

}


/* Next we must implement the "action" function ipc_action that will be called every time data is avalible on the socket.
In our example this function should set up communication and set the variable ipc_open to 1 if everything is ok.
As we are not to listen for data from the socket we do not need to add the new socket to the socket parser in olsrd.  */

void
ipc_action(int fd)
{
  struct sockaddr_in pin;
  size_t addrlen;
  char *addr;  

  if ((ipc_connection = accept(ipc_socket, (struct sockaddr *)  &pin, &addrlen)) == -1)
    {
      perror("IPC accept");
      exit(1);
    }
  else
    {
      addr = inet_ntoa(pin.sin_addr);
      if(ntohl(pin.sin_addr.s_addr) != INADDR_LOOPBACK)
      {
        printf("Front end-connection from foregin host(%s) not allowed!\n", addr);
	close(ipc_connection);
	return;
      }
      else
      {
        ipc_open = 1;
	printf("POWER: Connection from %s\n",addr);
      }
    }

}
/* Here we check that the incoming connection is initiated from our host. If so we set up the connection and set ipc_open to 1. */


// function that writes data to the socket
int
ipc_send(char *data, int size)
{
   if(!ipc_open)
     return 0;
   if (send(ipc_connection, data, size, MSG_NOSIGNAL) < 0) 
    {
      printf("(OUTPUT)IPC connection lost!\n");
      close(ipc_connection);
      ipc_open = 0;
      return -1;
    }

  return 1;
}
// Notice that ipc_open is reset to 0 if the connection is lost.




 
 
 




